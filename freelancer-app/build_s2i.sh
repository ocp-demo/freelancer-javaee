#!/bin/sh
PROJECT=$(oc project -q)
APPLICATION_NAME=freelancer-javaee
TEMPLATE=eap73-basic-s2i 
SOURCE_REPOSITORY_URL=https://gitlab.com/ocp-demo/freelancer-javaee.git
SOURCE_REPOSITORY_REF=master
CONTEXT_DIR=freelancer-app
oc new-app --template=$TEMPLATE \
-p APPLICATION_NAME=$APPLICATION_NAME \
-p SOURCE_REPOSITORY_URL=$SOURCE_REPOSITORY_URL \
-p SOURCE_REPOSITORY_REF=$SOURCE_REPOSITORY_REF \
-p CONTEXT_DIR=$CONTEXT_DIR \
-p GALLEON_PROVISION_LAYERS=datasources-web-server,web-clustering \
-e DISABLE_EMBEDDED_JMS_BROKER=true \
--env-file=runtime_artifact/datasource.env \
--labels=app=$APPLICATION_NAME,deploymentconfig=$APPLICATION_NAME,version=1.0.0,app.kubernetes.io/name=jboss \
-n $PROJECT
#oc new-app --template=eap72-basic-s2i -p APPLICATION_NAME=helloworld-custom-datasource 
# -p SOURCE_REPOSITORY_URL=https://<your-git-repository>/jboss-openshift-examples.git -p SOURCE_REPOSITORY_REF=master -p CONTEXT_DIR=helloworld-custom-datasource_by-env --env-file=datasource.env
