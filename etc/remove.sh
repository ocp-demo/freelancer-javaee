#!/bin/sh
APP=freelancer-javaee
#APP=freelancer-base
oc delete bc $APP
oc delete bc $APP-build-artifacts
oc delete dc $APP
oc delete svc $APP
oc delete svc $APP-ping
oc delete is $APP
oc delete is $APP-build-artifacts
oc delete route $APP
