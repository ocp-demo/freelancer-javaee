# Sample Java EE on JBoss EAP

![banner](images/banner.png)

<!-- TOC -->

- [Sample Java EE on JBoss EAP](#sample-java-ee-on-jboss-eap)
  - [Prerequisite](#prerequisite)
  - [Project Directory to support EAP on OpenShift](#project-directory-to-support-eap-on-openshift)
  - [Deployment from Git with Source-to-Image (S2I)](#deployment-from-git-with-source-to-image-s2i)

<!-- /TOC -->
Sample RESTful API application with database connection to PostgreSQL 

## Prerequisite
- Update imagestream for EAP 7.3
```bash
for resource in \
  eap73-amq-persistent-s2i.json \
  eap73-amq-s2i.json \
  eap73-basic-s2i.json \
  eap73-https-s2i.json \
  eap73-image-stream.json \
  eap73-sso-s2i.json \
  eap73-starter-s2i.json \
  eap73-third-party-db-s2i.json \
  eap73-tx-recovery-s2i.json \
  eap73-openjdk11-amq-persistent-s2i.json \
  eap73-openjdk11-amq-s2i.json \
  eap73-openjdk11-basic-s2i.json \
  eap73-openjdk11-https-s2i.json \
  eap73-openjdk11-image-stream.json \
  eap73-openjdk11-sso-s2i.json \
  eap73-openjdk11-starter-s2i.json \
  eap73-openjdk11-third-party-db-s2i.json \
  eap73-openjdk11-tx-recovery-s2i.json
do
  oc replace -n openshift --force -f \
https://raw.githubusercontent.com/jboss-container-images/jboss-eap-7-openshift-image/eap73/templates/${resource}
done
```
- Freelancer Application need PostgreSQL to store data. Shell script to deploy ephemeral Postgres with initial data.

```bash
cd etc
./create_postgresql.sh
```
- Sample output
```bash
Create PostgreSQL for Freelancer Service
secret/freelancer-db created
service/freelancer-db created
deploymentconfig.apps.openshift.io/freelancer-db created
Wait for pod to start ...
Deployment config "freelancer-db" waiting on image update
Deployment config "freelancer-db" waiting on image update
Waiting for latest deployment config spec to be observed by the controller loop...
Waiting for rollout to finish: 0 out of 1 new replicas have been updated...
Waiting for rollout to finish: 0 out of 1 new replicas have been updated...
Waiting for rollout to finish: 0 out of 1 new replicas have been updated...
Waiting for rollout to finish: 0 of 1 updated replicas are available...
Waiting for latest deployment config spec to be observed by the controller loop...
replication controller "freelancer-db-1" successfully rolled out
NAME                    READY   STATUS    RESTARTS   AGE
freelancer-db-1-plv6c   1/1     Running   0          63s
Freelancer Database Pod: freelancer-db-1-plv6c
building file list ... done
sql/
sql/data.sql
sql/schema.sql

sent 1576 bytes  received 70 bytes  1097.33 bytes/sec
total size is 1403  speedup is 0.85
DROP TABLE IF EXISTS FREELANCER_SKILLS;
DROP TABLE
DROP TABLE IF EXISTS FREELANCER;
DROP TABLE
CREATE TABLE FREELANCER (
  ...
```

## Project Directory to support EAP on OpenShift

To support EAP configuration with EAP to deploy on OpenShift. We need configuration as below
```bash
.
│ 
├── .s2i
│   └── environment
├── pom.xml
├── runtime_artifact
│   └── datasource.env
├── s2i_artifact
│   ├── drivers.env
│   ├── install.sh
│   └── modules
│       └── org
│           └── postgresql
│               └── custom
│                   └── main
│                       ├── module.xml
│                       └── postgresql-42.2.5.jar
├── src
     ├─ ...source code here ...
```

- s2i_artifact contains configuration of JDBC drivers and module
  - drivers.env contains configuration of JDBC driver and module
  - modules directory is configured with same layout as EAP's module
```bash
DRIVERS=POSTGRES
POSTGRES_DRIVER_NAME=postgres
POSTGRES_DRIVER_MODULE=org.postgresql.custom
POSTGRES_DRIVER_CLASS=org.postgresql.Driver
POSTGRES_XA_DATASOURCE_CLASS=org.postgresql.xa.PGXADataSource
```
- runtime_artifact contains configuration of Datasource in key/value format.
```property
DB_SERVICE_PREFIX_MAPPING=FreelancerDS-POSTGRES=DS1
DS1_URL=jdbc:postgresql://freelancer-db:5432/sampledb?socketTimeout=310
DS1_JNDI=java:jboss/datasources/freelancer
DS1_DRIVER=postgres
DS1_USERNAME=dbuser
DS1_PASSWORD=password
DS1_MAX_POOL_SIZE=20
DS1_MIN_POOL_SIZE=20
DS1_CONNECTION_CHECKER=org.jboss.jca.adapters.jdbc.extensions.postgres.PostgreSQLValidConnectionChecker
DS1_EXCEPTION_SORTER=org.jboss.jca.adapters.jdbc.extensions.postgres.PostgreSQLExceptionSorter
DS1_NONXA=true
```

## Deployment from Git with Source-to-Image (S2I)

```bash
cd freelancer-app
oc new-app --template=eap73-basic-s2i  \
-p APPLICATION_NAME=freelancer-javaee \
-p SOURCE_REPOSITORY_URL=https://gitlab.com/ocp-demo/freelancer-javaee.git \
-p SOURCE_REPOSITORY_REF=master \
-p CONTEXT_DIR=freelancer-app \
-p GALLEON_PROVISION_LAYERS=datasources-web-server,web-clustering \
-e DISABLE_EMBEDDED_JMS_BROKER=true \
--env-file=runtime_artifact/datasource.env \
--labels=app=freelancer-javaee,deploymentconfig=freelancer-javaee,version=1.0.0,app.kubernetes.io/name=jboss
```

<!-- ## Deployment form local directory
- Create EAP base image with Postgresql JDBC module and datasource configuration with S2I
```
PROJECT=$(oc project -q)
APPLICATION_NAME=freelancer-base
TEMPLATE=eap73-basic-s2i 
SOURCE_REPOSITORY_URL=https://gitlab.com/ocp-demo/freelancer-javaee.git
SOURCE_REPOSITORY_REF=master
CONTEXT_DIR=eap-base-for-freelancer
oc new-app --template=$TEMPLATE \
-p APPLICATION_NAME=$APPLICATION_NAME \
-p SOURCE_REPOSITORY_URL=$SOURCE_REPOSITORY_URL \
-p SOURCE_REPOSITORY_REF=$SOURCE_REPOSITORY_REF \
-p CONTEXT_DIR=$CONTEXT_DIR \
-p GALLEON_PROVISION_LAYERS=datasources-web-server,web-clustering \
-e DISABLE_EMBEDDED_JMS_BROKER=true \
--env-file=runtime_artifact/datasource.env \
-n $PROJECT
```
- Create build config
```bash
oc new-build --binary=true \
--name=freelancer-binary -l app=freelancer-binary \ 
--image-stream=freelancer-base:latest
```
- Start build from local binary file (WAR file)
```bash
mvn clean package -DskipTests=true
oc start-build freelancer \
--from-file=deployments/ROOT.war \
--follow
```
- Tag imagestream with current application's version
```bash
oc tag freelancer-binary:latest freelancer-binary:1.0.0
```
- Create new deployment (application)
```bash
oc new-app freelancer-binary:1.0.0 \
--labels=app=freelancer-binary,deploymentconfig=freelancer-binary,version=1.0.0,app.kubernetes.io/name=jboss
```
- Pause rollout to config EAP
```bash
oc rollout pause dc  freelancer
```
- Config deployment with environment variables to support for clustering
```bash
PROJECT=$(oc project -q)
oc set env dc freelancer \
JGROUPS_PING_PROTOCOL=kubernetes.KUBE_PING \
KUBERNETES_NAMESPACE=${PROJECT} \
KUBERNETES_LABELS=app=freelancer-binary,version=1.0.0
oc set env dc freelancer DISABLE_EMBEDDED_JMS_BROKER=true
```
- Resume rollout 
```bash
oc rollout resume dc  freelancer-binary
```
- Create route
```bash
oc expose svc/freelancer-binary
```
- Display route URL
```bash
oc get route freelancer-binary
``` -->